/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.greenhorn;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configure based on book "RESTful Java with JAX-RS 2.0, 2nd Edition.pdf" page
 * 206.
 *
 * @author greenhorn
 */
@ApplicationPath("/webapi/*")
public class ApplicationBind extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        HashSet<Class<?>> set = new HashSet<Class<?>>();
        set.add(MyResource.class);
        return set;
    }

}
